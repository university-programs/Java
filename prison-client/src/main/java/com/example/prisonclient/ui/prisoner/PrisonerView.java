package com.example.prisonclient.ui.prisoner;

import com.example.prisonclient.model.PrisonerDto;
import com.example.prisonclient.ui.View;
import org.springframework.stereotype.Component;

@Component
public class PrisonerView extends View {
    public void print(PrisonerDto prisoner) {
        System.out.println("Id: " + prisoner.getIdPrisoner());
        System.out.println("name: " + prisoner.getName());
        System.out.println("regime: " + prisoner.getRegime());
        System.out.println("id cell: " + prisoner.getIdCell());
        System.out.print("Watchers: [");
        prisoner.getIdWatched().forEach(System.out::println);
        System.out.println(" ]");
        System.out.println("-----------------------------");
    }
}
