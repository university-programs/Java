package com.example.prisonclient.ui.position;

import com.example.prisonclient.data.WorkPosClient;
import com.example.prisonclient.model.GuardDto;
import com.example.prisonclient.model.WorkPosDto;
import com.example.prisonclient.ui.guard.GuardView;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.web.reactive.function.client.WebClientException;

@ShellComponent
public class ConsolePosition {

    private final WorkPosClient workPosClient;
    private final PositionView positionView;
    private final GuardView guardView;

    public ConsolePosition(WorkPosClient workPosClient, PositionView positionView, GuardView guardView) {
        this.workPosClient = workPosClient;
        this.positionView = positionView;
        this.guardView = guardView;
    }

    @ShellMethod("Create a new guard: String name, Long minimal salary")
    public void createPosition(String name, Long minSalary){
        try{
            var pos = new WorkPosDto(name, minSalary);
            positionView.print(workPosClient.create(pos));
        } catch (WebClientException e){
            positionView.printError(e, "position", "create");
        }
    }

    @ShellMethod("Delete position by id")
    public void deletePosition(String id) {
        try {
            workPosClient.delete(id);
        } catch (WebClientException e) {
            positionView.printError(e, "position", "delete");
        }
    }

    @ShellMethod("Get position by id")
    public void getPositionById(String id) {
        try {
            positionView.print(workPosClient.getById(id));
        } catch (WebClientException e) {
            positionView.printError(e, "position", "find");
        }
    }

    @ShellMethod("Update position: String name, Long minSalary")
    public void updatePosition(String name, Long minSalary){
        try{
            var pos = new WorkPosDto(name, minSalary);
            positionView.print(workPosClient.update(name, pos));
        } catch (WebClientException e){
            positionView.printError(e, "position", "update");
        }
    }

    @ShellMethod("Get all guards on the position:")
    public void readAllGuards(String id) {
        try {
            workPosClient.readAllGuards(id).forEach(guardView::print);
        } catch (WebClientException e) {
            positionView.printError(e, "position", "guards read");
        }
    }
}
