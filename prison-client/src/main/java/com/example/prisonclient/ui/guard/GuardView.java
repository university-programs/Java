package com.example.prisonclient.ui.guard;

import com.example.prisonclient.model.GuardDto;
import com.example.prisonclient.ui.View;
import org.springframework.stereotype.Component;

@Component
public class GuardView extends View {
    public void print(GuardDto guard) {
        System.out.println("Id: " + guard.getIdGuard());
        System.out.println("name: " + guard.getName());
        System.out.println("idWoringPosition: " + guard.getIdWorkingPosition());
        System.out.println("salary: " + guard.getSalary());
        System.out.print("Observed: [");
        guard.getIdObserved().forEach(System.out::println);
        System.out.println(" ]");
        System.out.println("-----------------------------");
    }
}
