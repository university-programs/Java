package com.example.prisonclient.ui.guard;

import com.example.prisonclient.data.GuardClient;
import com.example.prisonclient.model.GuardDto;
import com.example.prisonclient.ui.prisoner.PrisonerView;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.web.reactive.function.client.WebClientException;

@ShellComponent
public class ConsoleGuard {

    private final GuardClient guardClient;
    private final GuardView guardView;
    private final PrisonerView prisonerView;

    public ConsoleGuard(GuardClient guardClient, GuardView guardView, PrisonerView prisonerView) {
        this.guardClient = guardClient;
        this.guardView = guardView;
        this.prisonerView = prisonerView;
    }

    @ShellMethod("Create a new guard: String name, String idPos, Long salary")
    public void createGuard(String name, String idPos, Long salary){
        try{
            var guard = new GuardDto(name, idPos, salary);
            guardView.print(guardClient.create(guard));
        } catch (WebClientException e){
            guardView.printError(e, "guard", "create");
        }
    }

    @ShellMethod("Delete guard by id")
    public void deleteGuard(Long id) {
        try {
            guardClient.delete(id);
        } catch (WebClientException e) {
            guardView.printError(e, "guard", "delete");
        }
    }

    @ShellMethod("Get guard by id")
    public void getGuardById(Long id) {
        try {
            guardView.print(guardClient.getById(id));
        } catch (WebClientException e) {
            guardView.printError(e, "guard", "find");
        }
    }

    @ShellMethod("Update guard: Long id, String name, Long salary")
    public void updateGuard(Long id, String name, Long salary){
        try{
            var guard = new GuardDto(name, guardClient.getById(id).getIdWorkingPosition(), salary);
            guardView.print(guardClient.update(id, guard));
        } catch (WebClientException e){
            guardView.printError(e, "guard", "update");
        }
    }

    @ShellMethod("Get prisoners observed guard with id:")
    public void readAllObserved(Long id) {
        try {
            guardClient.readAllObserved(id).forEach(prisonerView::print);
        } catch (WebClientException e) {
            guardView.printError(e, "guard", "observed read");
        }
    }

    @ShellMethod("Add a new observed:Long idGuard, Long idPrisoner ")
    public void addObserved(Long idGuard, Long idPrisoner) {
        try {
            guardView .print(guardClient.addObserved(idGuard, idPrisoner));
        } catch (WebClientException e) {
            guardView.printError(e, "guard or prisoner", "prisoner add");
        }
    }

    @ShellMethod("Delete the observed: Long idGuard, Long idPrisoner ")
    public void deleteObserved(Long idGuard, Long idPrisoner) {
        try {
            guardView .print(guardClient.deleteObserved(idGuard, idPrisoner));
        } catch (WebClientException e) {
            guardView.printError(e, "guard or prisoner", "delete");
        }
    }

    @ShellMethod("Add the position to guard: Long idGuard, String idPos")
    public void addPosition(Long idGuard, String idPos) {
        try {
            guardView .print(guardClient.addPositionToGuard(idGuard, idPos));
        } catch (WebClientException e) {
            guardView.printError(e, "guard or position", "position add");
        }
    }
}
