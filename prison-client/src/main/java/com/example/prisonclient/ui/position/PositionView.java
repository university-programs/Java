package com.example.prisonclient.ui.position;

import com.example.prisonclient.model.PrisonerDto;
import com.example.prisonclient.model.WorkPosDto;
import com.example.prisonclient.ui.View;
import org.springframework.stereotype.Component;

@Component
public class PositionView extends View {
    public void print(WorkPosDto wp) {
        System.out.println("Position name: " + wp.getPositionName());
        System.out.println("Minimal salary: " + wp.getMinSalary());
        System.out.print("Guards: [");
        wp.getIdSecurityGuards().forEach(System.out::println);
        System.out.println(" ]");
        System.out.println("-----------------------------");
    }
}
