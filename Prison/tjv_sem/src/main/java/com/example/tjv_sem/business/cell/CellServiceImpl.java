package com.example.tjv_sem.business.cell;

import com.example.tjv_sem.api.exception.EntityNotFoundException;
import com.example.tjv_sem.api.exception.EntityStateException;
import com.example.tjv_sem.dao.CellJpaRepository;
import com.example.tjv_sem.domain.Cell;
import com.example.tjv_sem.domain.Prisoner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CellServiceImpl implements CellService {

    private CellJpaRepository cellJpaRepository;
    private final Double MIN_SIZE = 8d;

    @Autowired
    public CellServiceImpl(CellJpaRepository cellJpaRepository) {
        this.cellJpaRepository = cellJpaRepository;
    }

    public CellServiceImpl() {
    }

    @Override
    public Cell create(Cell cell) {
        if(cell.getSize() < MIN_SIZE)
            throw new EntityStateException("The cell is too small");
        if (cell.getIdCell() != null && cellJpaRepository.existsById(cell.getIdCell()))
            throw new EntityStateException("Ceil with id " + cell.getIdCell() + " already exists");
        return cellJpaRepository.save(cell);
    }

    @Override
    public Cell update(Long id, Cell cell) {
        Cell ceilToUpdate = readByID(id);
        if(cell.getSize() < MIN_SIZE)
            throw new EntityStateException("The cell is too small");
        ceilToUpdate.setComfort(cell.getComfort());
        ceilToUpdate.setSize(cell.getSize());
        return cellJpaRepository.save(ceilToUpdate);
    }

    @Override
    public Cell readByID(Long id) {
        return cellJpaRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Ceil with id " + id + " not found."));
    }

    @Override
    public void deleteById(Long id) {
        if(! readByID(id).getPrisoners().isEmpty())
            throw new EntityStateException("The cell is not empty");
        cellJpaRepository.deleteById(id);
    }

    @Override
    public List<Prisoner> readPrisonersInCellById(Long id){
        return readByID(id).getPrisoners();
    }

    @Override
    public List<Cell> findCellsBySalary(Long salary) {
        return cellJpaRepository.findCellsBySalary(salary);
    }
}
