package com.example.tjv_sem.business.guard;

import com.example.tjv_sem.api.exception.EntityNotFoundException;
import com.example.tjv_sem.api.exception.EntityStateException;
import com.example.tjv_sem.dao.PrisonerJpaRepository;
import com.example.tjv_sem.dao.SecurityGuardJpaRepository;
import com.example.tjv_sem.dao.WorkingPosJpaRepository;
import com.example.tjv_sem.domain.Prisoner;
import com.example.tjv_sem.domain.SecurityGuard;
import com.example.tjv_sem.domain.WorkingPosition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Component
public class SecurityGuardServiceImpl implements SecurityGuardService{

    private SecurityGuardJpaRepository securityGuardJpaRepository;
    private PrisonerJpaRepository prisonerJpaRepository;
    private WorkingPosJpaRepository workingPosJpaRepository;

    @Autowired
    public SecurityGuardServiceImpl(SecurityGuardJpaRepository securityGuardJpaRepository,
                                    PrisonerJpaRepository prisonerJpaRepository,
                                    WorkingPosJpaRepository workingPosJpaRepository) {
        this.securityGuardJpaRepository = securityGuardJpaRepository;
        this.prisonerJpaRepository = prisonerJpaRepository;
        this.workingPosJpaRepository = workingPosJpaRepository;
    }

    public SecurityGuardServiceImpl() {}

    @Override
    public SecurityGuard create(SecurityGuard securityGuard) {
        if (securityGuard.getIdGuard() != null && securityGuardJpaRepository.existsById(securityGuard.getIdGuard()))
            throw new EntityStateException ("Guard with id " + securityGuard.getIdGuard() + " already exists.");
        if(securityGuard.getWorkingPosition().getMinSalary() > securityGuard.getSalary())
            throw new EntityStateException("Salary " + securityGuard.getSalary() + " is too low");
        return securityGuardJpaRepository.save(securityGuard);
    }

    @Override
    public SecurityGuard update(Long id, SecurityGuard securityGuard) {
        if(securityGuard.getWorkingPosition().getMinSalary() > securityGuard.getSalary())
            throw new EntityStateException("Salary " + securityGuard.getSalary() + " is too low");
        SecurityGuard securityGuardToUpdate = readByID(id);
        securityGuardToUpdate.setName(securityGuard.getName());
        securityGuardToUpdate.setSalary(securityGuard.getSalary());
        return securityGuardJpaRepository.save(securityGuardToUpdate);
    }

    @Override
    public SecurityGuard readByID(Long id) {
        return securityGuardJpaRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Security guard with id " + id + " not found."));
    }

    @Override
    public void deleteById(Long id) {
        long RATIO = 10;
        if(securityGuardJpaRepository.count() > 0 &&
                (securityGuardJpaRepository.count() - 1) * RATIO < prisonerJpaRepository.count())
            throw new EntityStateException("Too many prisoners, can't be removed");
        securityGuardJpaRepository.deleteById(readByID(id).getIdGuard());
    }

    @Override
    public SecurityGuard addObserve(Long idSecurityGuard, Long idPrisoner) {
        SecurityGuard securityGuard = readByID(idSecurityGuard);
        Prisoner prisoner = prisonerJpaRepository.findById(idPrisoner)
                .orElseThrow(() -> new EntityNotFoundException("Prisoner with id " + idPrisoner + " not found."));

        if(securityGuard.getObserved().contains(prisoner))
            return securityGuard;
        securityGuard.getObserved().add(prisoner);
        return securityGuardJpaRepository.save(securityGuard);
    }

    @Override
    public SecurityGuard deleteObserve(Long idSecurityGuard, Long idPrisoner) {
        SecurityGuard securityGuard = readByID(idSecurityGuard);
        Prisoner prisoner = prisonerJpaRepository.findById(idPrisoner)
                .orElseThrow(() -> new EntityNotFoundException("Prisoner with id " + idPrisoner + " not found."));

        securityGuard.getObserved().remove(prisoner);
        return securityGuardJpaRepository.save(securityGuard);
    }

    @Override
    public List<Prisoner> readObserved(Long id) {
        return readByID(id).getObserved();
    }

    @Override
    public SecurityGuard changeWorkPos(Long id, String pos) {
        SecurityGuard securityGuard = readByID(id);
        WorkingPosition workingPosition = workingPosJpaRepository.findById(pos)
                .orElseThrow(() -> new EntityNotFoundException("Work. position with id " + pos + " not found."));
        if(securityGuard.getSalary() < workingPosition.getMinSalary())
            securityGuard.setSalary(workingPosition.getMinSalary());
        securityGuard.setWorkingPosition(workingPosition);
        return securityGuardJpaRepository.save(securityGuard);
    }
}
