package com.example.tjv_sem.api.controller;

import com.example.tjv_sem.api.controller.dto.CellDto;
import com.example.tjv_sem.api.controller.dto.PrisonerDto;
import com.example.tjv_sem.api.controller.dto.converter.CellConverter;
import com.example.tjv_sem.api.controller.dto.converter.PrisonerConverter;
import com.example.tjv_sem.business.cell.CellService;
import com.example.tjv_sem.domain.Cell;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cells")
public class CellController implements ControllerCRUD<Long, Cell, CellDto> {

    private final CellService cellService;
    private final CellConverter cellConverter;
    private final PrisonerConverter prisonerConverter;

    public CellController(CellService cellService, CellConverter cellConverter, PrisonerConverter prisonerConverter) {
        this.cellService = cellService;
        this.cellConverter = cellConverter;
        this.prisonerConverter = prisonerConverter;
    }

    @Override
    @GetMapping("/{id}")
    public CellDto getById(@PathVariable Long id){
        return cellConverter.toDto(cellService.readByID(id));
    }

    @Override
    @PostMapping
    public CellDto create(@RequestBody CellDto cellDto){
        return cellConverter.toDto(cellService.create(cellConverter.toEntity(cellDto)));
    }

    @Override
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id){
        cellService.deleteById(id);
    }

    @Override
    @PutMapping("/{id}")
    public CellDto update(@PathVariable Long id, @RequestBody CellDto cellDto){
        return cellConverter.toDto(cellService.update(id, cellConverter.toEntity(cellDto)));
    }

    @GetMapping("/{id}/prisoners")
    public List<PrisonerDto> readAllPrisoners(@PathVariable Long id){
        return prisonerConverter.toDtoCol(cellService.readPrisonersInCellById(id));
    }

    @GetMapping("/find/{salary}")
    public List<CellDto> findCellsBySalary(@PathVariable Long salary) {
        return cellConverter.toDtoCol(cellService.findCellsBySalary(salary));
    }
}
