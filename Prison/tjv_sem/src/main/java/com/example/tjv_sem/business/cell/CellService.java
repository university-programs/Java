package com.example.tjv_sem.business.cell;

import com.example.tjv_sem.domain.Cell;
import com.example.tjv_sem.domain.Prisoner;

import java.util.List;

public interface CellService {
    Cell create(Cell ceil);
    Cell update(Long id, Cell cell);
    Cell readByID(Long id);
    void deleteById(Long id);
    List<Prisoner> readPrisonersInCellById(Long id);
    List<Cell> findCellsBySalary(Long salary);
}
