package com.example.tjv_sem.api.controller.dto;

import java.util.ArrayList;
import java.util.List;

public class CellDto {
    private Long idCell;
    private Double size;
    private Double comfort;
    private List<Long> idPrisoners = new ArrayList<>();

    public Long getIdCell() {
        return idCell;
    }

    public void setIdCell(Long idCell) {
        this.idCell = idCell;
    }

    public Double getSize() {
        return size;
    }

    public void setSize(Double size) {
        this.size = size;
    }

    public Double getComfort() {
        return comfort;
    }

    public void setComfort(Double comfort) {
        this.comfort = comfort;
    }

    public List<Long> getIdPrisoners() {
        return idPrisoners;
    }

    public void setIdPrisoners(List<Long> idPrisoners) {
        this.idPrisoners = idPrisoners;
    }
}
