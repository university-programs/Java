package com.example.tjv_sem.api.controller;

import com.example.tjv_sem.api.controller.dto.GuardDto;
import com.example.tjv_sem.api.controller.dto.WorkPosDto;
import com.example.tjv_sem.api.controller.dto.converter.GuardConverter;
import com.example.tjv_sem.api.controller.dto.converter.WorkPosConverter;
import com.example.tjv_sem.business.workpos.WorkingPosService;
import com.example.tjv_sem.domain.WorkingPosition;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/positions")
public class WorkPosController implements ControllerCRUD<String, WorkingPosition, WorkPosDto>{

    private final WorkingPosService workingPosService;
    private final WorkPosConverter workPosConverter;
    private final GuardConverter guardConverter;


    public WorkPosController(WorkingPosService workingPosService, WorkPosConverter workPosConverter, GuardConverter guardConverter) {
        this.workingPosService = workingPosService;
        this.workPosConverter = workPosConverter;
        this.guardConverter = guardConverter;
    }


    @Override
    @GetMapping("/{id}")
    public WorkPosDto getById(@PathVariable String id) {
        return workPosConverter.toDto(workingPosService.readByID(id));
    }

    @Override
    @PostMapping
    public WorkPosDto create(@RequestBody WorkPosDto workPosDto) {
        return workPosConverter.toDto(workingPosService.create(workPosConverter.toEntity(workPosDto)));
    }

    @Override
    @DeleteMapping("/{id}")
    public void delete(@PathVariable String id) {
        workingPosService.deleteById(id);
    }

    @Override
    @PutMapping("/{id}")
    public WorkPosDto update(@PathVariable String id,@RequestBody WorkPosDto workPosDto) {
        return workPosConverter.toDto(workingPosService.update(id, workPosConverter.toEntity(workPosDto)));
    }

    @GetMapping("/{id}/guards")
    public List<GuardDto> readAllGuards(@PathVariable String id){
        return guardConverter.toDtoCol(workingPosService.readAllGuards(id));
    }

}
