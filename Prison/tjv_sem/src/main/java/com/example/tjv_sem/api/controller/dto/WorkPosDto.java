package com.example.tjv_sem.api.controller.dto;
import java.util.List;

public class WorkPosDto {
    private String positionName;
    private Long minSalary;
    private List<Long> idSecurityGuards;

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public Long getMinSalary() {
        return minSalary;
    }

    public void setMinSalary(Long minSalary) {
        this.minSalary = minSalary;
    }

    public List<Long> getIdSecurityGuards() {
        return idSecurityGuards;
    }

    public void setIdSecurityGuards(List<Long> idSecurityGuards) {
        this.idSecurityGuards = idSecurityGuards;
    }
}
