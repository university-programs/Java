package com.example.tjv_sem.api.controller;

import com.example.tjv_sem.api.controller.dto.GuardDto;
import com.example.tjv_sem.api.controller.dto.PrisonerDto;
import com.example.tjv_sem.api.controller.dto.converter.GuardConverter;
import com.example.tjv_sem.api.controller.dto.converter.PrisonerConverter;
import com.example.tjv_sem.business.prisoner.PrisonerService;
import com.example.tjv_sem.domain.Prisoner;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/prisoners")
public class PrisonerController implements ControllerCRUD<Long, Prisoner, PrisonerDto> {

    private final PrisonerService prisonerService;
    private final PrisonerConverter prisonerConverter;
    private final GuardConverter guardConverter;

    public PrisonerController(PrisonerService prisonerService, PrisonerConverter prisonerConverter, GuardConverter guardConverter) {
        this.prisonerService = prisonerService;
        this.prisonerConverter = prisonerConverter;
        this.guardConverter = guardConverter;
    }

    @Override
    @GetMapping("/{id}")
    public PrisonerDto getById(@PathVariable Long id){
        return prisonerConverter.toDto(prisonerService.readByID(id));
    }

    @Override
    @PostMapping
    public PrisonerDto create(@RequestBody PrisonerDto prisonerDto){
        return prisonerConverter.toDto(prisonerService.create(prisonerConverter.toEntity(prisonerDto)));
    }

    @Override
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id){
        prisonerService.deleteById(id);
    }

    @Override
    @PutMapping("/{id}")
    public PrisonerDto update(@PathVariable Long id,@RequestBody PrisonerDto prisonerDto){
        return prisonerConverter.toDto(prisonerService.update(id, prisonerConverter.toEntity(prisonerDto)));
    }

    @GetMapping("/{id}/guards")
    List<GuardDto> readAllWatched(@PathVariable Long id){
        return guardConverter.toDtoCol(prisonerService.readAllWatched(id));
    }

    @PostMapping("/{idPrisoner}/cells/{idCell}")
    public PrisonerDto addPrisonerToCell(@PathVariable Long idCell, @PathVariable Long idPrisoner){
        return prisonerConverter.toDto(prisonerService.addPrisonerToCell(idPrisoner, idCell));
    }

}