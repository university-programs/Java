package com.example.tjv_sem.api.controller.dto.converter;

import com.example.tjv_sem.api.controller.dto.CellDto;
import com.example.tjv_sem.api.exception.EntityStateException;
import com.example.tjv_sem.domain.Cell;
import com.example.tjv_sem.domain.Prisoner;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Component
public record CellConverter(){

    public CellDto toDto(Cell cell) {
        var dto = new CellDto();
        dto.setComfort(cell.getComfort());
        dto.setIdCell(cell.getIdCell());
        dto.setSize(cell.getSize());
        dto.setIdPrisoners(cell.getPrisoners().stream().map(Prisoner::getIdPrisoner).collect(Collectors.toList()));
        return dto;
    }

    public Cell toEntity(CellDto cellDto) {
        if(cellDto.getComfort() == null || cellDto.getSize() == null)
            throw new EntityStateException("Not null argument is null");
        return new Cell(cellDto.getIdCell(), cellDto.getSize(), cellDto.getComfort());
    }

    public List<CellDto> toDtoCol(Collection<Cell> cells){
        return cells.stream().map(this::toDto).toList();
    }

    public List<Cell> toEntityCol(Collection<CellDto> cellsDto){
        return cellsDto.stream().map(this::toEntity).toList();
    }
}

