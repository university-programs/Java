package com.example.tjv_sem.domain;

import javax.persistence.*;
import java.util.*;

@Entity
public class SecurityGuard {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idGuard;
    private String name;
    @ManyToOne
    @JoinColumn(name = "workingPosition_id")
    private WorkingPosition workingPosition;
    private Long salary;

    @ManyToMany
    @JoinTable(
            name = "security_prisoner",
            joinColumns = {@JoinColumn(name = "security_id")},
            inverseJoinColumns = {@JoinColumn(name = "prisoner_id")}
    )
    private List<Prisoner> observed = new ArrayList<>();

    public SecurityGuard(Long idGuard , String name, WorkingPosition workingPosition, Long salary) {
        this.idGuard = idGuard;
        this.name = Objects.requireNonNull(name);
        this.workingPosition = Objects.requireNonNull(workingPosition);
        this.salary = Objects.requireNonNull(salary);
    }

    public SecurityGuard() {}

    public Long getIdGuard() {
        return idGuard;
    }

    public void setIdGuard(Long idGuard) {
        this.idGuard = idGuard;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public WorkingPosition getWorkingPosition() {
        return workingPosition;
    }

    public void setWorkingPosition(WorkingPosition workingPosition) {
        this.workingPosition = workingPosition;
    }

    public Long getSalary() {
        return salary;
    }

    public void setSalary(Long salary) {
        this.salary = salary;
    }

    public List<Prisoner> getObserved() {
        return observed;
    }

    public void setObserved(List<Prisoner> observed) {
        this.observed = observed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SecurityGuard that = (SecurityGuard) o;
        return idGuard.equals(that.idGuard);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idGuard);
    }
}
