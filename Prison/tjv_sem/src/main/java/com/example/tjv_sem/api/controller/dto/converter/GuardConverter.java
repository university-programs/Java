package com.example.tjv_sem.api.controller.dto.converter;

import com.example.tjv_sem.api.controller.dto.GuardDto;
import com.example.tjv_sem.api.exception.EntityStateException;
import com.example.tjv_sem.business.workpos.WorkingPosService;
import com.example.tjv_sem.domain.Prisoner;
import com.example.tjv_sem.domain.SecurityGuard;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Component
public record GuardConverter(WorkingPosService workingPosService) {

    public GuardDto toDto(SecurityGuard securityGuard) {
        var dto = new GuardDto();
        dto.setIdGuard(securityGuard.getIdGuard());
        dto.setName(securityGuard.getName());
        dto.setSalary(securityGuard.getSalary());
        dto.setIdWorkingPosition(securityGuard.getWorkingPosition().getPositionName());
        dto.setIdObserved(securityGuard.getObserved().stream().map(Prisoner::getIdPrisoner).collect(Collectors.toList()));
        return dto;
    }

    public SecurityGuard toEntity(GuardDto guardDto) {
        if(guardDto.getIdWorkingPosition() == null || guardDto.getName() == null || guardDto.getSalary() == null)
            throw new EntityStateException("Not null argument is null");
        return new SecurityGuard(guardDto.getIdGuard(), guardDto.getName(),
                workingPosService.readByID(guardDto.getIdWorkingPosition()), guardDto.getSalary());
    }

    public List<GuardDto> toDtoCol(Collection<SecurityGuard> guards){
        return guards.stream().map(this::toDto).toList();
    }

    public List<SecurityGuard> toEntityCol(Collection<GuardDto> guardsDto){
        return guardsDto.stream().map(this::toEntity).toList();
    }

}
