package com.example.tjv_sem.api.controller;

import com.example.tjv_sem.api.controller.dto.converter.GuardConverter;
import com.example.tjv_sem.api.controller.dto.converter.PrisonerConverter;
import com.example.tjv_sem.api.exception.EntityNotFoundException;
import com.example.tjv_sem.api.exception.EntityStateException;
import com.example.tjv_sem.business.cell.CellService;
import com.example.tjv_sem.business.prisoner.PrisonerService;
import com.example.tjv_sem.business.workpos.WorkingPosService;
import com.example.tjv_sem.domain.Cell;
import com.example.tjv_sem.domain.Prisoner;
import com.example.tjv_sem.domain.SecurityGuard;
import com.example.tjv_sem.domain.WorkingPosition;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(PrisonerController.class)
class PrisonerControllerTest {

    @MockBean
    PrisonerService prsService;
    @MockBean
    WorkingPosService wpService;
    @MockBean
    CellService cellService;
    @Autowired
    PrisonerConverter prisonerConverter;
    @Autowired
    GuardConverter guardConverter;
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    MockMvc mockMvc;

    @TestConfiguration
    static class TestConfig {

        @Bean
        public GuardConverter guardConverter(WorkingPosService wp) {
            return new GuardConverter(wp);
        }

        @Bean
        public PrisonerConverter prisonerConverter(CellService cellService){
            return new PrisonerConverter(cellService);
        }

    }
    @Test
    void testGetById() throws Exception {

        Cell cell = new Cell(1L, 10d, 10d);
        Prisoner prisoner = new Prisoner(1L, "Anton", "Normal", cell);

        Mockito.when(prsService.readByID(1L)).thenReturn(prisoner);
        Mockito.when(prsService.readByID(not(eq(1L)))).thenThrow(EntityNotFoundException.class);
        mockMvc.perform(get("/prisoners/1")) //HTTP request
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.idPrisoner", Matchers.is(1)))
                .andExpect(jsonPath("$.name", Matchers.is("Anton")))
                .andExpect(jsonPath("$.regime", Matchers.is("Normal")))
                .andExpect(jsonPath("$.idCell", Matchers.is(1)))
                .andExpect(jsonPath("$.idWatched").isEmpty());

        mockMvc.perform(get("/prisoners/404"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testCreateExisting() throws Exception {
        Mockito.when(cellService.readByID(1L))
                .thenReturn(new Cell(1L, 10d,10d));
        doThrow(new EntityStateException()).when(prsService).create(any(Prisoner.class));

        mockMvc.perform(post("/prisoners")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\": \"Anton\",\"regime\": \"Normal\",\"idCell\": 1}"))
                .andExpect(status().isConflict());
    }

    @Test
    public void testCreate() throws Exception {
        Cell cell = new Cell(1L, 10d, 10d);
        Prisoner prisoner = new Prisoner(1L, "Anton", "Normal", cell);
        Mockito.when(cellService.readByID(1L)).thenReturn(cell);
        Mockito.when(prsService.create(prisoner)).thenReturn(prisoner);
        mockMvc.perform(post("/prisoners")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"idPrisoner\": 1,\"name\": \"Anton\",\"regime\": \"Normal\",\"idCell\": 1}"))
                .andExpect(jsonPath("$.idPrisoner", Matchers.is(1)))
                .andExpect(jsonPath("$.name", Matchers.is("Anton")))
                .andExpect(jsonPath("$.regime", Matchers.is("Normal")))
                .andExpect(jsonPath("$.idCell", Matchers.is(1)))
                .andExpect(jsonPath("$.idWatched").isEmpty());


        ArgumentCaptor<Prisoner> argumentCaptor = ArgumentCaptor.forClass(Prisoner.class);
        Mockito.verify(prsService, Mockito.times(1)).create(argumentCaptor.capture());
        var prisonerProvidedToService = argumentCaptor.getValue();
        assertEquals(1, prisonerProvidedToService.getIdPrisoner());
        assertEquals("Anton", prisonerProvidedToService.getName());
        assertEquals(cell, prisonerProvidedToService.getCell());
        assertEquals("Normal", prisonerProvidedToService.getRegime());
        assertEquals(new ArrayList<>() ,prisonerProvidedToService.getWatched());
    }

    @Test
    void testDelete() throws Exception{

        Mockito.when(prsService.readByID(not(eq(1L)))).thenThrow(EntityNotFoundException.class);

        mockMvc.perform(get("/prisoners/404"))
                .andExpect(status().isNotFound());
        //... deleteById never called
        verify(prsService, never()).deleteById(any());

        //Delete return HTTP status Ok
        mockMvc.perform(delete("/prisoners/1"))
                .andExpect(status().isOk());
        //...deleteById was called
        verify(prsService, times(1)).deleteById(1L);
    }

    @Test
    void testUpdate() throws Exception{
        Cell cell = new Cell(1L, 10d, 10d);
        Prisoner prisoner = new Prisoner(1L, "Anton", "Normal", cell);
        Prisoner prisonerUpd = new Prisoner(1L, "Fernando", "Normal2", cell);

        Mockito.when(cellService.readByID(1L)).thenReturn(cell);
        Mockito.when(prsService.update(1L, prisoner)).thenReturn(prisonerUpd);

        mockMvc.perform(put("/prisoners/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"idPrisoner\": 1,\"name\": \"Fernando\",\"regime\": \"Normal2\",\"idCell\": 1}"))
                .andExpect(jsonPath("$.idPrisoner", Matchers.is(1)))
                .andExpect(jsonPath("$.name", Matchers.is("Fernando")))
                .andExpect(jsonPath("$.regime", Matchers.is("Normal2")))
                .andExpect(jsonPath("$.idCell", Matchers.is(1)))
                .andExpect(jsonPath("$.idWatched").isEmpty());

        //Check with class ArgumentCaptor, that method create was call 1x with correctly data
        ArgumentCaptor<Prisoner> argumentCaptor = ArgumentCaptor.forClass(Prisoner.class);
        Mockito.verify(prsService, Mockito.times(1)).update(eq(1L), argumentCaptor.capture());
        var prisonerProvidedToService = argumentCaptor.getValue();
        assertEquals(1, prisonerProvidedToService.getIdPrisoner());
        assertEquals("Fernando", prisonerProvidedToService.getName());
        assertEquals(cell, prisonerProvidedToService.getCell());
        assertEquals("Normal2", prisonerProvidedToService.getRegime());
        assertEquals(new ArrayList<>() ,prisonerProvidedToService.getWatched());
    }

    @Test
    public void testUpdateNotExisting() throws Exception {
        Mockito.when(cellService.readByID(1L))
                .thenReturn(new Cell(1L, 10d,10d));
        doThrow(new EntityNotFoundException()).when(prsService).update(any(Long.class), any(Prisoner.class));

        mockMvc.perform(put("/prisoners/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"idPrisoner\": 1,\"name\": \"Anton\",\"regime\": \"Normal\",\"idCell\": 1}"))
                .andExpect(status().isNotFound());
    }

    @Test
    void testReadAllWatched() throws Exception{
        WorkingPosition wp = new WorkingPosition("Normal", 1000L, new ArrayList<>());
        SecurityGuard guard = new SecurityGuard(1L, "Bob", wp, 5000L);
        SecurityGuard guard2 = new SecurityGuard(2L, "Bobik", wp, 5000L);
        Cell cell = new Cell(1L, 10d, 10d);
        Prisoner p = new Prisoner(1L, "Antonio", "Normal", cell);
        guard.setObserved(List.of(p));
        guard2.setObserved(List.of(p));
        List<SecurityGuard> guards = List.of(guard, guard2);
        p.setWatched(guards);
        Mockito.when(cellService.readByID(1L)).thenReturn(cell);
        Mockito.when(prsService.readByID(1L)).thenReturn(p);
        Mockito.when(prsService.readByID(not(eq(1L)))).thenThrow(EntityNotFoundException.class);
        Mockito.when(prsService.readAllWatched(1L)).thenReturn(guards);
        Mockito.when(prsService.readAllWatched(not(eq(1L)))).thenThrow(EntityNotFoundException.class);

        mockMvc.perform(get("/prisoners/404/guards"))
                .andExpect(status().isNotFound());

        mockMvc.perform(get("/prisoners/1/guards"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(2)))
                .andExpect(jsonPath("$[0].idGuard", Matchers.is(1)))
                .andExpect(jsonPath("$[0].name", Matchers.is("Bob")))
                .andExpect(jsonPath("$[0].idWorkingPosition", Matchers.is("Normal")))
                .andExpect(jsonPath("$[0].salary", Matchers.is(5000)))
                .andExpect(jsonPath("$[0].idObserved").value(1))
                .andExpect(jsonPath("$[1].idGuard", Matchers.is(2)))
                .andExpect(jsonPath("$[1].name", Matchers.is("Bobik")))
                .andExpect(jsonPath("$[1].idWorkingPosition", Matchers.is("Normal")))
                .andExpect(jsonPath("$[1].salary", Matchers.is(5000)))
                .andExpect(jsonPath("$[1].idObserved").value(1));
    }

    @Test
    void testAddPrisonerToCell() throws Exception{
        Cell cell = new Cell(1L, 10d, 10d);
        Cell cellTo = new Cell(2L, 10d, 10d);
        Prisoner p = new Prisoner(1L, "Antonio", "Normal", cell);
        Prisoner pAfter = new Prisoner(1L, "Antonio", "Normal", cellTo);
        Mockito.when(cellService.readByID(1L)).thenReturn(cell);
        Mockito.when(cellService.readByID(2L)).thenReturn(cellTo);
        Mockito.when(prsService.readByID(1L)).thenReturn(p);
        Mockito.when(prsService.addPrisonerToCell(1L, 2L)).thenReturn(pAfter);
        Mockito.when(prsService.addPrisonerToCell(not(eq(1L)), any())).thenThrow(EntityNotFoundException.class);

        mockMvc.perform(post("/prisoners/1/cells/2"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.idPrisoner", Matchers.is(1)))
                .andExpect(jsonPath("$.name", Matchers.is("Antonio")))
                .andExpect(jsonPath("$.regime", Matchers.is("Normal")))
                .andExpect(jsonPath("$.idCell", Matchers.is(2)))
                .andExpect(jsonPath("$.idWatched").isEmpty());

        mockMvc.perform(post("/prisoners/404/cells/1"))
                .andExpect(status().isNotFound());
    }
}