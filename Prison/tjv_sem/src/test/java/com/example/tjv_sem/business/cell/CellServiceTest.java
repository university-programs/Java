package com.example.tjv_sem.business.cell;

import com.example.tjv_sem.api.exception.EntityNotFoundException;
import com.example.tjv_sem.api.exception.EntityStateException;
import com.example.tjv_sem.dao.CellJpaRepository;
import com.example.tjv_sem.domain.Cell;
import com.example.tjv_sem.domain.Prisoner;
import com.example.tjv_sem.domain.SecurityGuard;
import com.example.tjv_sem.domain.WorkingPosition;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class CellServiceTest {

    @Autowired
    private CellService cellService;
    @MockBean
    private CellJpaRepository cellRep;
    private final Cell cellFound = new Cell(1L, 10d, 10d);
    private final Cell cellErr = new Cell(999L, 1d, 10d);
    private final Cell cellNotExists = new Cell(404L, 404d, 404d);
    private final Cell cellExists = new Cell(2L, 999d, 999d);
    private final Cell cellUpd = new Cell(1L, 81.3, 10.3);
    @BeforeEach
    void setUp(){
        Mockito.when(cellRep.findById(cellFound.getIdCell())).thenReturn(Optional.of(cellFound));
        Mockito.when(cellRep.findById(cellErr.getIdCell())).thenThrow(EntityStateException.class);
        Mockito.when(cellRep.existsById(cellExists.getIdCell())).thenThrow(EntityStateException.class);
        Mockito.when(cellRep.existsById(cellNotExists.getIdCell())).thenThrow(EntityNotFoundException.class);
        Mockito.when(cellRep.findById(cellNotExists.getIdCell())).thenThrow(EntityNotFoundException.class);
        Mockito.when(cellRep.findCellsBySalary(5000L)).thenReturn(List.of(cellFound));
    }
    @Test
    void create() {
        cellService.create(cellFound);
        Assertions.assertEquals(Optional.of(cellFound), cellRep.findById(cellFound.getIdCell()));
        Mockito.verify(cellRep, Mockito.times(1)).save(cellFound);
        Assertions.assertThrows(EntityStateException.class, ()-> cellService.create(cellErr));
        Mockito.verify(cellRep, Mockito.times(0)).save(cellErr);
    }
    @Test
    void createExists() {
        Assertions.assertThrows(EntityStateException.class, () -> cellService.create(cellExists));
        Mockito.verify(cellRep, Mockito.times(0)).save(cellExists);
    }

    @Test
    void update() {
        Assertions.assertTrue(cellUpd.getPrisoners().isEmpty());
        assertEquals(cellFound.getIdCell(), cellUpd.getIdCell());
        assertNotEquals(cellFound.getSize(), cellUpd.getSize());
        assertNotEquals(cellFound.getComfort(), cellUpd.getComfort());
        cellService.update(cellFound.getIdCell(), cellUpd);
        Mockito.verify(cellRep, Mockito.times(1)).save(cellUpd);
        cellErr.setIdCell(cellFound.getIdCell());
        Assertions.assertThrows(EntityStateException.class,
                ()-> cellService.update(cellErr.getIdCell(), cellErr));
    }

    @Test
    void updateNotExists() {
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> cellService.update(cellNotExists.getIdCell(), cellNotExists));
        Mockito.verify(cellRep, Mockito.times(0)).save(cellNotExists);
    }

    @Test
    void readByIdNotExistsAndExists() {
        Assertions.assertEquals(cellService.readByID(cellFound.getIdCell()), cellFound);
        Assertions.assertThrows(EntityNotFoundException.class,() -> cellService.readByID(cellNotExists.getIdCell()));
    }

    @Test
    void deleteById() {
        cellService.create(cellFound);
        cellService.deleteById(cellFound.getIdCell());
        Mockito.verify(cellRep, Mockito.times(1)).deleteById(cellFound.getIdCell());
    }

    @Test
    void deleteByIdNotExists() {
        Assertions.assertThrows(EntityNotFoundException.class, ()-> cellService.deleteById(cellNotExists.getIdCell()));
        Mockito.verify(cellRep, Mockito.times(0)).deleteById(cellNotExists.getIdCell());
    }

    @Test
    void readPrisonersInCellById() {
        Prisoner p1 = new Prisoner(1L, "Anton", "Normal", cellFound);
        Prisoner p2 = new Prisoner(2L, "Antonio", "Normal", cellFound);
        var prisoners = List.of(p1,p2);
        cellFound.setPrisoners(prisoners);
        Assertions.assertEquals(cellService.readPrisonersInCellById(cellFound.getIdCell()), prisoners);
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> cellService.readPrisonersInCellById(cellNotExists.getIdCell()));
    }

    @Test
    void findCellsBySalary() {
        SecurityGuard g1 = new SecurityGuard(1L, "Anton", new WorkingPosition(), 5000L);
        Prisoner p1 = new Prisoner(1L, "Anton", "Normal", cellFound);
        Prisoner p2 = new Prisoner(2L, "Antonio", "Normal", cellFound);
        p1.setWatched(List.of(g1));
        g1.setObserved(List.of(p1));
        p1.setWatched(List.of(g1));
        cellFound.setPrisoners(List.of(p1, p2));

        Assertions.assertEquals(cellService.findCellsBySalary(5000L), List.of(cellFound));
        Assertions.assertTrue(cellService.findCellsBySalary(9000L).isEmpty());
    }
}