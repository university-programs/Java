package com.example.tjv_sem.api.controller;

import com.example.tjv_sem.api.controller.dto.converter.GuardConverter;
import com.example.tjv_sem.api.controller.dto.converter.WorkPosConverter;
import com.example.tjv_sem.api.exception.EntityNotFoundException;
import com.example.tjv_sem.api.exception.EntityStateException;
import com.example.tjv_sem.business.guard.SecurityGuardService;
import com.example.tjv_sem.business.workpos.WorkingPosService;
import com.example.tjv_sem.domain.SecurityGuard;
import com.example.tjv_sem.domain.WorkingPosition;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(WorkPosController.class)
class WorkPosControllerTest {

    @MockBean
    WorkingPosService workingPosService;

    @MockBean
    SecurityGuardService guardService;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    MockMvc mockMvc;

    @Autowired
    WorkPosConverter workPosConverter;

    @Autowired
    GuardConverter guardConverter;

    @TestConfiguration
    static class TestConfig {

        @Bean
        public WorkPosConverter workPosConverter(SecurityGuardService sgs) {
            return new WorkPosConverter(sgs);
        }

        @Bean
        public GuardConverter guardConverter(WorkingPosService wps){
            return new GuardConverter(wps);
        }

    }
    @Test
    void testGetById() throws Exception{
        WorkingPosition wp = new WorkingPosition("security", 5000L, new ArrayList<>());

        Mockito.when(workingPosService.readByID("security")).thenReturn(wp);
        Mockito.when(workingPosService.readByID(not(eq("security")))).thenThrow(EntityNotFoundException.class);
        mockMvc.perform(get("/positions/security"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.positionName", Matchers.is("security")))
                .andExpect(jsonPath("$.minSalary", Matchers.is(5000)))
                .andExpect(jsonPath("$.idSecurityGuards").isEmpty());


        mockMvc.perform(get("/positions/superStar"))
                .andExpect(status().isNotFound());
    }
    @Test
    public void testCreateExisting() throws Exception {
        doThrow(new EntityStateException()).when(workingPosService).create(any(WorkingPosition.class));

        mockMvc.perform(post("/positions")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"positionName\": \"Pos2\"," +
                                "    \"minSalary\": 900" +
                                "}"))
                .andExpect(status().isConflict());
    }

    @Test
    public void testCreate() throws Exception {
        WorkingPosition wp = new WorkingPosition("security", 5000L, new ArrayList<>());

        Mockito.when(workingPosService.create(wp)).thenReturn(wp);
        mockMvc.perform(post("/positions")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"positionName\": \"security\"," +
                                "    \"minSalary\": 5000" +
                                "}"))
                .andExpect(status().isOk()) // ожидаем, что статс ок
                .andExpect(jsonPath("$.positionName", Matchers.is("security")))
                .andExpect(jsonPath("$.minSalary", Matchers.is(5000)))
                .andExpect(jsonPath("$.idSecurityGuards").isEmpty());

        ArgumentCaptor<WorkingPosition> argumentCaptor = ArgumentCaptor.forClass(WorkingPosition.class);
        Mockito.verify(workingPosService, Mockito.times(1)).create(argumentCaptor.capture());
        WorkingPosition wpProvidedToService = argumentCaptor.getValue();
        assertEquals("security", wpProvidedToService.getPositionName());
        assertEquals(5000L, wpProvidedToService.getMinSalary());
        assertEquals(new ArrayList<>() ,wpProvidedToService.getSecurityGuards());
    }
    @Test
    void testDelete() throws Exception{
        WorkingPosition wp = new WorkingPosition("security", 5000L, new ArrayList<>());

        Mockito.when(workingPosService.readByID("security")).thenReturn(wp);
        Mockito.when(workingPosService.readByID(not(eq("security")))).thenThrow(EntityNotFoundException.class);


        mockMvc.perform(get("/positions/404"))
                .andExpect(status().isNotFound());

        verify(workingPosService, never()).deleteById(any());

        mockMvc.perform(delete("/positions/security"))
                .andExpect(status().isOk());

        verify(workingPosService, times(1)).deleteById("security");
    }
    @Test
    void testUpdate() throws Exception{
        WorkingPosition wp = new WorkingPosition("security", 5000L, new ArrayList<>());
        WorkingPosition wpu = new WorkingPosition("security", 9999L, new ArrayList<>());

        Mockito.when(workingPosService.readByID("security")).thenReturn(wp);
        Mockito.when(workingPosService.readByID(not(eq("security")))).thenThrow(EntityNotFoundException.class);
        Mockito.when(workingPosService.update("security", wp)).thenReturn(wpu);
        mockMvc.perform(put("/positions/security")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"positionName\": \"security\"," +
                                "    \"minSalary\": 9999" +
                                "}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.positionName", Matchers.is("security")))
                .andExpect(jsonPath("$.minSalary", Matchers.is(9999)))
                .andExpect(jsonPath("$.idSecurityGuards").isEmpty());

        ArgumentCaptor<WorkingPosition> argumentCaptor = ArgumentCaptor.forClass(WorkingPosition.class);
        Mockito.verify(workingPosService, Mockito.times(1))
                .update( eq("security"), argumentCaptor.capture());
        WorkingPosition wpProvidedToService = argumentCaptor.getValue();
        assertEquals("security", wpProvidedToService.getPositionName());
        assertEquals(9999L, wpProvidedToService.getMinSalary());
        assertEquals(new ArrayList<>() ,wpProvidedToService.getSecurityGuards());
    }

    @Test
    public void testUpdateNotExisting() throws Exception {
        doThrow(new EntityNotFoundException()).when(workingPosService).update(any(String.class), any(WorkingPosition.class));

        mockMvc.perform(put("/positions/404")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"positionName\": \"security\"," +
                                "    \"minSalary\": 5000" +
                                "}"))
                .andExpect(status().isNotFound());
    }
    @Test
    void readAllGuards() throws Exception{
        WorkingPosition wp = new WorkingPosition("security", 5000L, new ArrayList<>());
        var g1 = new SecurityGuard(1L, "Barbos", wp, 9999L);
        var g2 = new SecurityGuard(2L, "Wolfeschlegelsteinhausenbergerdorff", wp, 10000L);

        List<SecurityGuard> guards = List.of(g1, g2);
        wp.setSecurityGuards(guards);
        Mockito.when(workingPosService.readByID("security")).thenReturn(wp);
        Mockito.when(workingPosService.readByID(not(eq("security")))).thenThrow(EntityNotFoundException.class);
        Mockito.when(workingPosService.readAllGuards("security")).thenReturn(guards);
        mockMvc.perform(get("/positions/security/guards"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(2)))
                .andExpect(jsonPath("$[0].idGuard", Matchers.is(1)))
                .andExpect(jsonPath("$[0].name", Matchers.is("Barbos")))
                .andExpect(jsonPath("$[0].salary", Matchers.is(9999)))
                .andExpect(jsonPath("$[0].idWorkingPosition", Matchers.is("security")))
                .andExpect(jsonPath("$[0].idObserved").isEmpty())
                .andExpect(jsonPath("$[1].idGuard", Matchers.is(2)))
                .andExpect(jsonPath("$[1].name", Matchers.is("Wolfeschlegelsteinhausenbergerdorff")))
                .andExpect(jsonPath("$[1].salary", Matchers.is(10000)))
                .andExpect(jsonPath("$[1].idWorkingPosition", Matchers.is("security")))
                .andExpect(jsonPath("$[1].idObserved").isEmpty());
    }
}