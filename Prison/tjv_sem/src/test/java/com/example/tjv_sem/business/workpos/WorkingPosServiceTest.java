package com.example.tjv_sem.business.workpos;

import com.example.tjv_sem.api.exception.EntityNotFoundException;
import com.example.tjv_sem.api.exception.EntityStateException;
import com.example.tjv_sem.dao.SecurityGuardJpaRepository;
import com.example.tjv_sem.dao.WorkingPosJpaRepository;
import com.example.tjv_sem.domain.Cell;
import com.example.tjv_sem.domain.Prisoner;
import com.example.tjv_sem.domain.SecurityGuard;
import com.example.tjv_sem.domain.WorkingPosition;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class WorkingPosServiceTest {

    @Autowired
    WorkingPosService service;
    @MockBean
    WorkingPosJpaRepository wpRep;
    @MockBean
    SecurityGuardJpaRepository sgRep;

    private final WorkingPosition wpFound = new WorkingPosition("PosTest", 500L, new ArrayList<>());
    private final WorkingPosition wpUpd= new WorkingPosition("PosTest", 5000L, new ArrayList<>());
    private final WorkingPosition wpExists = new WorkingPosition("PosTestEx", 500L, new ArrayList<>());
    private final WorkingPosition wpNotExists = new WorkingPosition("404", 500L, new ArrayList<>());
    @BeforeEach
    void setUp() {
        Mockito.when(wpRep.findById(wpFound.getPositionName())).thenReturn(Optional.of(wpFound));
        Mockito.when(wpRep.findById(wpNotExists.getPositionName())).thenThrow(EntityNotFoundException.class);
        Mockito.when(wpRep.existsById(wpNotExists.getPositionName())).thenThrow(EntityStateException.class);
        Mockito.when(wpRep.existsById(wpExists.getPositionName())).thenReturn(true);
    }

    @Test
    void create() {
        service.create(wpFound);
        Assertions.assertEquals(Optional.of(wpFound), wpRep.findById(wpFound.getPositionName()));
        Mockito.verify(wpRep, Mockito.times(1)).save(wpFound);
        //PositionName == null
        Assertions.assertThrows(EntityStateException.class, ()-> service.create(new WorkingPosition()));
    }
    @Test
    void createExists() {
        Assertions.assertThrows(EntityStateException.class, () -> service.create(wpExists));
        Mockito.verify(wpRep, Mockito.times(0)).save(wpExists);
    }

    @Test
    void update() {
        assertEquals(wpFound.getPositionName(), wpUpd.getPositionName());
        assertNotEquals(wpFound.getMinSalary(), wpUpd.getMinSalary());
        service.update(wpFound.getPositionName(), wpUpd);
        Mockito.verify(wpRep, Mockito.times(1)).save(wpUpd);
    }

    @Test
    void updateNotExists() {
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> service.update(wpNotExists.getPositionName(), wpNotExists));
        Mockito.verify(wpRep, Mockito.times(0)).save(wpNotExists);
    }

    @Test
    void readByID() {
        Assertions.assertEquals(service.readByID(wpFound.getPositionName()), wpFound);
        Assertions.assertThrows(EntityNotFoundException.class,() -> service.readByID(wpNotExists.getPositionName()));
    }

    @Test
    void deleteById() {
        service.create(wpFound);
        service.deleteById(wpFound.getPositionName());
        Mockito.verify(wpRep, Mockito.times(1)).deleteById(wpFound.getPositionName());
        service.create(wpFound);
        wpFound.setSecurityGuards(List.of(new SecurityGuard()));
        Assertions.assertThrows(EntityStateException.class,
                ()-> service.deleteById(wpFound.getPositionName()));
    }

    @Test
    void deleteByIdNotExists() {
        Assertions.assertThrows(EntityNotFoundException.class, ()-> service.deleteById(wpNotExists.getPositionName()));
        Mockito.verify(wpRep, Mockito.times(0)).deleteById(wpNotExists.getPositionName());
    }

    @Test
    void readAllGuards() {
        SecurityGuard g1 = new SecurityGuard(1L, "Anton", wpFound, 1000L);
        SecurityGuard g2 = new SecurityGuard(2L, "Antonio", wpFound, 1000L);
        var guards = List.of(g1,g2);
        wpFound.setSecurityGuards(guards);
        Assertions.assertEquals(service.readAllGuards(wpFound.getPositionName()), guards);
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> service.readAllGuards(wpNotExists.getPositionName()));
    }

}