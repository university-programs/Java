package com.example.tjv_sem.business.prisoner;

import com.example.tjv_sem.api.exception.EntityNotFoundException;
import com.example.tjv_sem.api.exception.EntityStateException;
import com.example.tjv_sem.business.cell.CellService;
import com.example.tjv_sem.dao.CellJpaRepository;
import com.example.tjv_sem.dao.PrisonerJpaRepository;
import com.example.tjv_sem.dao.SecurityGuardJpaRepository;
import com.example.tjv_sem.domain.Cell;
import com.example.tjv_sem.domain.Prisoner;
import com.example.tjv_sem.domain.SecurityGuard;
import com.example.tjv_sem.domain.WorkingPosition;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class PrisonerServiceTest {

    @Autowired
    PrisonerService prisonerService;
    @Autowired
    CellService cellService;
    @MockBean
    SecurityGuardJpaRepository sgRep;
    @MockBean
    PrisonerJpaRepository prisRep;
    @MockBean
    CellJpaRepository cellRep;

    private final Cell cell = new Cell(1L, 999d, 999d);
    private final Prisoner prisonerFound = new Prisoner(1L, "Anton", "Normal", cell);
    private final Prisoner prisonerToUpd = new Prisoner(1L, "Bobster", "Normal3", cell);
    private final Prisoner prisonerExists= new Prisoner(2L, "Antonio", "Normal", cell);
    private final Prisoner prisonerNotExists= new Prisoner(404L, "Antonio", "Normal", cell);
    private final Prisoner prisonerFoundErr = new Prisoner(88L, "Anton", "Normal2", cell);

    @BeforeEach
    void setUp() {
        Mockito.when(prisRep.findById(prisonerFound.getIdPrisoner())).thenReturn(Optional.of(prisonerFound));
        Mockito.when(cellRep.findById(cell.getIdCell())).thenReturn(Optional.of(cell));
        Mockito.when(prisRep.existsById(prisonerExists.getIdPrisoner())).thenThrow(EntityStateException.class);
        Mockito.when(prisRep.findById(prisonerFoundErr.getIdPrisoner())).thenReturn(Optional.of(prisonerFoundErr));
        Mockito.when(prisRep.findById(prisonerNotExists.getIdPrisoner())).thenThrow(EntityNotFoundException.class);
    }

    @Test
    void create() {
        cell.setPrisoners(List.of(prisonerFound, prisonerFoundErr));
        Mockito.when(sgRep.count()).thenReturn(Long.valueOf(1));
        Mockito.when(prisRep.count()).thenReturn(Long.valueOf(0));
        prisonerService.create(prisonerFound);
        Assertions.assertEquals(Optional.of(prisonerFound), prisRep.findById(prisonerFound.getIdPrisoner()));
        Mockito.verify(prisRep, Mockito.times(1)).save(prisonerFound);
        Assertions.assertThrows(EntityStateException.class, ()-> prisonerService.create(prisonerFoundErr));
    }
    @Test
    void createExists() {
        Assertions.assertThrows(EntityStateException.class, () -> prisonerService.create(prisonerExists));
        Mockito.verify(prisRep, Mockito.times(0)).save(prisonerExists);
    }

    @Test
    void update() {
        assertEquals(prisonerFound.getIdPrisoner(), prisonerToUpd.getIdPrisoner());
        assertNotEquals(prisonerFound.getName(), prisonerToUpd.getName());
        assertNotEquals(prisonerFound.getRegime(), prisonerToUpd.getRegime());
        prisonerService.update(prisonerFound.getIdPrisoner(), prisonerToUpd);
        Mockito.verify(prisRep, Mockito.times(1)).save(prisonerToUpd);
    }

    @Test
    void updateNotExists() {
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> prisonerService.update(prisonerNotExists.getIdPrisoner(), prisonerNotExists));
        Mockito.verify(prisRep, Mockito.times(0)).save(prisonerNotExists);
    }

    @Test
    void readByID() {
        Assertions.assertEquals(prisonerService.readByID(prisonerFound.getIdPrisoner()), prisonerFound);
        Assertions.assertThrows(EntityNotFoundException.class,() -> prisonerService.readByID(prisonerNotExists.getIdPrisoner()));
    }

    @Test
    void deleteById() {
        Mockito.when(sgRep.count()).thenReturn(Long.valueOf(1));
        Mockito.when(prisRep.count()).thenReturn(Long.valueOf(1));
        prisonerService.create(prisonerFound);
        prisonerService.deleteById(prisonerFound.getIdPrisoner());
        Mockito.verify(prisRep, Mockito.times(1)).deleteById(prisonerFound.getIdPrisoner());
    }

    @Test
    void deleteByIdNotExists() {
        Assertions.assertThrows(EntityNotFoundException.class, ()-> prisonerService.deleteById(prisonerNotExists.getIdPrisoner()));
        Mockito.verify(prisRep, Mockito.times(0)).deleteById(prisonerNotExists.getIdPrisoner());
    }

    @Test
    void readAllWatched() {
        WorkingPosition wp = new WorkingPosition("Test", 500L, new ArrayList<>());
        SecurityGuard g1 = new SecurityGuard(1L, "Anton", wp, 1000L);
        SecurityGuard g2 = new SecurityGuard(2L, "Antonio", wp, 1000L);
        var guards = List.of(g1,g2);
        prisonerFound.setWatched(guards);
        Assertions.assertEquals(prisonerService.readAllWatched(prisonerFound.getIdPrisoner()), guards);
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> prisonerService.readAllWatched(prisonerNotExists.getIdPrisoner()));
    }

    @Test
    void addPrisonerToCell() {
        cell.setPrisoners(List.of(new Prisoner(), new Prisoner(), new Prisoner(),new Prisoner(), new Prisoner(), new Prisoner()));
        Assertions.assertThrows(EntityStateException.class,
                ()-> prisonerService.addPrisonerToCell(prisonerFound.getIdPrisoner(), cell.getIdCell()));
        cell.setPrisoners(List.of(new Prisoner(9L, "Frederich", "TestNotSame", cell)));
        Assertions.assertThrows(EntityStateException.class,
                ()-> prisonerService.addPrisonerToCell(prisonerFound.getIdPrisoner(), cell.getIdCell()));
        cell.setPrisoners(new ArrayList<>());
        var cell2 = new Cell(999L, 10d, 10d);
        var p = prisonerFound;
        p.setCell(cell2);
        Mockito.when(prisRep.save(p)).thenReturn(p);
        Mockito.when(cellRep.findById(cell2.getIdCell())).thenReturn(Optional.of(cell2));
        Assertions.assertEquals(cell2,
                prisonerService.addPrisonerToCell(prisonerFound.getIdPrisoner(), cell2.getIdCell()).getCell());
    }

    @Test
    void addPrisonerToCellNotExists() {
        Assertions.assertThrows(EntityNotFoundException.class,
                ()-> prisonerService.addPrisonerToCell(prisonerNotExists.getIdPrisoner(), cell.getIdCell()));
        Mockito.verify(prisRep, Mockito.times(0)).save(prisonerNotExists);
        Cell notExCell = new Cell(404L, 10d, 111d);
        Mockito.when(cellRep.findById(notExCell.getIdCell())).thenThrow(EntityNotFoundException.class);
        Assertions.assertThrows(EntityNotFoundException.class,
                ()-> prisonerService.addPrisonerToCell(prisonerFound.getIdPrisoner(), notExCell.getIdCell()));
        Mockito.verify(prisRep, Mockito.times(0)).save(prisonerFound);
    }
}